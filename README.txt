Visitor Analytics integrates your Drupal site with https://www.visitor-analytics.io/ tracker. Statistics are available right inside Drupal admin as well.

INSTALLATION

You can install the module as usual. 

To configure the module, proceed to admin/config/services/visitor-analytics. You will be directed to register form which allows you to register at Visitor Analytics right from the admin page. When you are registered (or if you already have an account) check your mail to confirm registration and then proceed to admin/config/services/visitor-analytics/login to Log In.

After the first login your site will be registered automatically, your free trial will be activated and tracker script will be loaded from Visitor Analytics and embedded to each page.

After successful login tracking stats will be available on admin/config/services/visitor-analytics page.