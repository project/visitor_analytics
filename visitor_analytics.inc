<?php


/**
 *
 * Wrapper around drupal_http_request() to talk with the service API
 *
 * @param string $route
 * @param string $method
 * @param array $data
 * @param string $token if set, use in Authorization header
 * @return \stdClass response (if redirect didn't happen)
 */
function _visitor_analytics_http_request($route, $method, $data = array(), $token = '') {
  $options = array();
  $options['method'] = $method;

  if (!empty($data)) {
    $options['data'] = ($method == 'GET') ? drupal_http_build_query($data) : json_encode($data);
  }

  if ($token) {
    $options['headers'] = array(
      'Authorization' => 'Bearer ' . $token,
    );
  }

  $response = drupal_http_request(VISITOR_ANALYTICS_API_ENDPOINT . $route, $options);

  // check for errors that should lead to re-login
  if (!in_array($response->code, array(200, 201))) {
    $data = json_decode($response->data);
    if (in_array($data->error, array('AUTHENTICATION_EXPIRED_JWT_TOKEN', 'AUTHENTICATION_JWT_TOKEN_ENCODE', 'AUTHENTICATION_INVALID_JWT_TOKEN'))) {
      drupal_goto('admin/config/services/visitor-analytics/login');
    }
  }

  return $response;
}


/**
 * Admin page callback
 */
function _visitor_analytics_admin_page() {
  $visitor_analytics_data = variable_get('visitor_analytics_data', array());

  if (isset($visitor_analytics_data['token'])) {
    // check if we need to refresh the token (expired)
    if ((time() - $visitor_analytics_data['token_created']) > VISITOR_ANALYTICS_API_TOKEN_LIFETIME) {
      $response = _visitor_analytics_http_request('/api/drup/token-refresh', 'POST', array('refreshToken' => $visitor_analytics_data['refreshToken']));
      if ($response->code == 200) {
        // update token, refreshToken, userId
        $response_data = json_decode($response->data);
        $visitor_analytics_data['token'] = $response_data->token;
        $visitor_analytics_data['refreshToken'] = $response_data->refreshToken;
        $visitor_analytics_data['userId'] = $response_data->userId;
        variable_set('visitor_analytics_data', $visitor_analytics_data);
        // no need to process errors because all possible errors lead to login redirect
      }
    }
      
    // try to get website
    global $base_url;
    $response = _visitor_analytics_http_request('/api/websites', 'GET', array('url' => $base_url), $visitor_analytics_data['token']);
    $data = json_decode($response->data);
    if (($response->code != 200) && ($data->error == 'NOT_FOUND_WEBSITE')) {
      // try to register new site
      $response = _visitor_analytics_http_request('/api/websites', 'POST', array(
        'url' => $base_url,
        'timezone' => drupal_get_user_timezone(),
        'platform' => 'DRUPAL',
      ), $visitor_analytics_data['token']);

      $site_reg_data = json_decode($response->data);
      if ($response->code != 200) {
        // report error and go to login
        drupal_set_message(t('Failed registering the site. Please, try again.'), 'error');
        watchdog(t('Site registration falure. Response code is !code, error is !error', array('!code' => $response->code, 'error' => $site_reg_data->error)), 'visitor_analytics');
        drupal_goto('admin/config/services/visitor-analytics/login');
      } else {
        $visitor_analytics_data['website_id'] = $site_reg_data->id;
        variable_set('visitor_analytics_data', $visitor_analytics_data);
      }
    } else if ($response->code == 200) {
      $visitor_analytics_data['website_id'] = $data->id;
    }

    if (!isset($visitor_analytics_data['trialEndsAt'])) {
      // no free trial info yet, try to start free trial
      $response = _visitor_analytics_http_request('/api/websites/' . $visitor_analytics_data['website_id'] . '/subscriptions/free-trial', 'POST', array(), $visitor_analytics_data['token']);
      $response_data = json_decode($response->data);
      if ($response->code == 201) {
        $visitor_analytics_data['trialEndsAt'] = $response_data->trialEndsAt;
        variable_set('visitor_analytics_data', $visitor_analytics_data);
      } else {
        // report error
        drupal_set_message(t('Error starting free trial. Response code is !code, error is !error', array('!code' => $response->code, '!error' => $response_data->error)), 'error');
        watchdog(t('Error starting free trial. Response code is !code, error is !error', array('!code' => $response->code, '!error' => $response_data->error)), 'visitor_analytics');
        if ($response_data->error == 'ALREADY_USED_FREE_TRIAL') {
          // we need to set it though we don't know when it would end at this point
          // this can occur when you work with same site&account from different environments (localhosts)
          $visitor_analytics_data['trialEndsAt'] = 0;
          variable_set('visitor_analytics_data', $visitor_analytics_data);
        }
      }
    }

    if (isset($visitor_analytics_data['website_id']) && !isset($visitor_analytics_data['trackingCode'])) {
      // try to get tracking code
      $response = _visitor_analytics_http_request('/api/websites/' . $visitor_analytics_data['website_id'] . '/tracking-code', 'GET', array(), $visitor_analytics_data['token']);
      $tracking_data = json_decode($response->data);
      if ($response->code == 200) {
        $visitor_analytics_data['trackingCode'] = $tracking_data->trackingCode;
        variable_set('visitor_analytics_data', $visitor_analytics_data);
        drupal_set_message(t('Tracking code loaded successfully'));
      } else {
        // report error
        drupal_set_message(t('Tracking code loading falure. Response code is !code, error is !error', array('!code' => $response->code, '!error' => $tracking_data->error)), 'error');
        watchdog(t('Tracking code loading falure. Response code is !code, error is !error', array('!code' => $response->code, '!error' => $tracking_data->error)), 'visitor_analytics');
      }
    }

    if (isset($visitor_analytics_data['token']) && isset($visitor_analytics_data['website_id']) && isset($visitor_analytics_data['userId'])) {
      return theme('visitor_analytics_iframe', array(
        'token' => $visitor_analytics_data['token'],
        'website_id' => $visitor_analytics_data['website_id'],
        'userId' => $visitor_analytics_data['userId'],
      ));
    }

    return t('Something went wrong.');
  }

  // if no tokens but email exists, redirect to login
  if (isset($visitor_analytics_data['email'])) {
    drupal_goto('admin/config/services/visitor-analytics/login');
  }

  // If no data exists, redirect to register
  drupal_goto('admin/config/services/visitor-analytics/register');
}


/**
 *
 * Visitor Analytics register form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function visitor_analytics_register_form($form, &$form_state) {
  $form['firstName'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#required' => TRUE,
  );

  $form['lastName'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#required' => TRUE,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
  );

  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
  );

  $form['companyName'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#required' => FALSE,
  );

  $terms_of_use = l(t('Terms Of Use'), 'https://www.visitor-analytics.io/standard-terms-and-conditions', array('absolute' => TRUE));
  $dpa = l(t('Data Processing Agreement'), 'https://www.visitor-analytics.io/standard-data-processing-agreement', array('absolute' => TRUE));
  $form['terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('I confirm that I have read, consent and agree to Visitor Analytics !terms and !dpa.', array(
      '!terms' => $terms_of_use,
      '!dpa' => $dpa,
    )),
    '#required' => TRUE,
  );

  $form['newsletterSubscribed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep me updated with Visitor Analytics news and offers and tips&tricks about analytics.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create account'),
  );

  drupal_add_css(drupal_get_path('module', 'visitor_analytics') . '/css/visitor-analytics.css', 'file');

  return $form;
}


/**
 *
 * Form validation
 *
 * @param $form
 * @param $form_state
 */
function visitor_analytics_register_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('Email must be valid.'));
  }
}


/**
 *
 * Form submission
 *
 * @param $form
 * @param $form_state
 */
function visitor_analytics_register_form_submit($form, &$form_state) {
  $data = array(
    'email' => $form_state['values']['email'],
    'password' => $form_state['values']['password'],
    'firstName' => $form_state['values']['firstName'],
    'lastName' => $form_state['values']['lastName'],
    'companyName' => $form_state['values']['companyName'],
    'privacyTermsAccepted' => $form_state['values']['terms'],
    'dpaAccepted' => $form_state['values']['terms'],
    'businessConditionAccepted' => $form_state['values']['terms'],
    'newsletterSubscribed' => $form_state['values']['newsletterSubscribed'],
    'source' => 'Drupal',
  );
  $response = _visitor_analytics_http_request('/api/users', 'POST', $data);

  if ($response->code == '201') {
    drupal_set_message(t('You have successfully registered at visitor-analytics.io. Please, check your mailbox for email confirmation letter.'));
    $_SESSION['visitor_analytics_email'] = $form_state['values']['email'];
    // new registration, we need to reset all config
    $visitor_analytics_data = array(
      'email' => $form_state['values']['email'],
    );
    variable_set('visitor_analytics_data', $visitor_analytics_data);
    // go to login
    $form_state['redirect'] = 'admin/config/services/visitor-analytics/login';
  } else {
    $data = json_decode($response->data);
    if ($data->error == 'ALREADY_EXISTING_USER') {
      drupal_set_message(t('User already registered. Please, log in.'), 'error');
      $_SESSION['visitor_analytics_email'] = $form_state['values']['email'];
      $form_state['redirect'] = 'admin/config/services/visitor-analytics/login';
    } else {
      drupal_set_message(t('Error while registering account (invalid data).'));
    }
  }
}


/**
 *
 * Visitor Analytics login form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function visitor_analytics_login_form($form, &$form_state) {
  $email = isset($_SESSION['visitor_analytics_email']) ? $_SESSION['visitor_analytics_email'] : '';
  unset($_SESSION['visitor_analytics_email']);
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#default_value' => $email,
  );

  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Login'),
  );

  drupal_add_css(drupal_get_path('module', 'visitor_analytics') . '/css/visitor-analytics.css', 'file');

  return $form;
}


/**
 *
 * Form validation
 *
 * @param $form
 * @param $form_state
 */
function visitor_analytics_login_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('Email must be valid.'));
  }
}


/**
 *
 * Form submission
 *
 * @param $form
 * @param $form_state
 */
function visitor_analytics_login_form_submit($form, &$form_state) {
  $data = array(
    'email' => $form_state['values']['email'],
    'password' => $form_state['values']['password'],
    'platform' => 'DRUPAL',
  );

  $response = _visitor_analytics_http_request('/api/drup/token', 'POST', $data);
  $response_data = json_decode($response->data);

  if ($response->code == 200) {
    // store data and redirect to main page
    $visitor_analytics_data = variable_get('visitor_analytics_data', array());
    $visitor_analytics_data['token'] = $response_data->token;
    $visitor_analytics_data['refreshToken'] = $response_data->refreshToken;
    $visitor_analytics_data['userId'] = $response_data->userId;
    $visitor_analytics_data['token_created'] = time();
    $visitor_analytics_data['email'] = $form_state['values']['email'];
    variable_set('visitor_analytics_data', $visitor_analytics_data);
    $form_state['redirect'] = 'admin/config/services/visitor-analytics';
  } else {
    if ($response_data->error == 'AUTHENTICATION_USER_PENDING') {
      drupal_set_message(t('You need to confirm registration. Please, check your email!'), 'error');
      $_SESSION['visitor_analytics_email'] = $form_state['values']['email'];
    } else if ($response_data->error == 'AUTHENTICATION_USER_NOT_FOUND') {
      drupal_set_message(t('User not found.'), 'error');
    } else if ($response_data->error == 'AUTHENTICATION_BAD_CREDENTIALS') {
      drupal_set_message(t('Login or password is incorrect.'), 'error');
      $_SESSION['visitor_analytics_email'] = $form_state['values']['email'];
    } else {
      drupal_set_message(t('Error while authenticating. Please try again.'), 'error');
    }
  }
}
