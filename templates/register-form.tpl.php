<?php
/**
 * Registration form for Visitor Analytics
 */

$path = drupal_get_path('module', 'visitor_analytics');
$form['firstName']['#title_display'] = 'invisible';
$form['firstName']['#theme_wrappers'] = array();
$form['firstName']['#attributes']['class'] = array('notification-input');
$form['lastName']['#title_display'] = 'invisible';
$form['lastName']['#theme_wrappers'] = array();
$form['lastName']['#attributes']['class'] = array('notification-input');
$form['email']['#title_display'] = 'invisible';
$form['email']['#theme_wrappers'] = array();
$form['email']['#attributes']['class'] = array('notification-input');
$form['companyName']['#title_display'] = 'invisible';
$form['companyName']['#theme_wrappers'] = array();
$form['companyName']['#attributes']['class'] = array('notification-input');
$form['password']['#title_display'] = 'invisible';
$form['password']['#theme_wrappers'] = array();
$form['password']['#attributes']['class'] = array('notification-input');
$form['submit']['#attributes']['class'] = array('button', 'theme-green');
?>
<div class="app landing-background">
  <div class="logo home-link">
    <img src="/<?php echo $path; ?>/images/va-logo.svg">
  </div>
  <div class="register-wrapper">
    <div class="box popup-box card-white">
      <h4 class="popup-title"><?php print t('Start using Visitor Analytics!'); ?></h4>
      <h3 class="popup-subtitle"><?php print t('It takes a minute to register and you will have all your stats at your fingertips.'); ?></h3>
      <div class="popup-content">
        <form class="register-form action_login">
          <div class="input-fields-area">
            <div class="two-inputs-container">
              <div class="custom-input">
                <img class="icon" src="/<?php echo $path; ?>/images/forms/user.svg">
                <label><?php print t('First Name'); ?></label>
                <?php print drupal_render($form['firstName']); ?>
              </div>
              <div class="custom-input">
                <img class="icon" src="/<?php echo $path; ?>/images/forms/user.svg">
                <label><?php print t('Last Name'); ?></label>
                <?php print drupal_render($form['lastName']); ?>
              </div>
            </div>
            <div class="custom-input">
              <img class="icon" src="/<?php echo $path; ?>/images/forms/message.svg">
              <label><?php print t('Email'); ?></label>
              <?php print drupal_render($form['email']); ?>
            </div>
            <div class="custom-input">
              <img class="icon" src="/<?php echo $path; ?>/images/forms/password.svg">
              <label><?php print t('Password'); ?></label>
              <?php print drupal_render($form['password']); ?>
            </div>
            <div class="custom-input">
              <img class="icon" src="/<?php echo $path; ?>/images/forms/bussiness.svg">
              <label><?php print t('Company Name'); ?></label>
              <?php print drupal_render($form['companyName']); ?>
            </div>
          </div>
          <div class="checkboxes-area">
            <div class="custom-checkbox">
              <?php print drupal_render($form['terms']); ?>
            </div>
          </div>
          <div class="checkboxes-area" style="margin-top:0;">
            <div class="custom-checkbox">
              <?php print drupal_render($form['newsletterSubscribed']); ?>
            </div>
          </div>
          <div class="button-area">
            <?php print drupal_render($form['submit']); ?>
            <p style="margin-top: 0;font-weight: 400; text-align: center;"><?php print t('*30-Days Free Trial. No credit card required. No strings attached.'); ?></p>
            <p align="center">
              <span class="secondary-text account-exists"><?php print t('Already have a Visitor Analytics account?'); ?></span>
              <span class="secondary-text hyperlink-green">
                <span><a class="bold to_login_page" href="/admin/config/services/visitor-analytics/login"><?php print t('Log in now'); ?></a></span>
              </span>
            </p>
          </div>
        </form>
      </div>
      <?php print drupal_render_children($form); ?>
    </div>
  </div>
</div>
