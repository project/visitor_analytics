<?php
  /**
   * Visitor Analytics iframe
   * Vars:
   *  - token
   *  - website_id
   *  - userId
   */
?>
<div style="position:relative; height: calc(100vh - 165px);">
  <iframe
    src="https://drupal7-dashboard.va-endpoint.com/?token=<?php print $token; ?>&wid=<?php print $website_id; ?>&uid=<?php print $userId; ?>&source=DRUPAL7"
    scrolling="no" frameborder="0"
    style="position: absolute; height: 100%; width: 100%;"/>
</div>