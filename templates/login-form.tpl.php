<?php
/**
 * Login form for Visitor Analytics
 */

$path = drupal_get_path('module', 'visitor_analytics');
$form['email']['#title_display'] = 'invisible';
$form['email']['#theme_wrappers'] = array();
$form['email']['#attributes']['class'] = array('notification-input');
$form['password']['#title_display'] = 'invisible';
$form['password']['#theme_wrappers'] = array();
$form['password']['#attributes']['class'] = array('notification-input');
$form['submit']['#attributes']['class'] = array('button', 'theme-green');
?>
<div class="app landing-background">
  <div class="logo home-link">
    <img src="/<?php echo $path; ?>/images/va-logo.svg">
  </div>
  <div class="register-wrapper">
    <div class="box popup-box card-white">
      <h4 class="popup-title"><?php print t('Visitor Analytics Login'); ?></h4>
      <div class="popup-content">
        <form class="register-form action_login">
          <div class="input-fields-area">
            <div class="custom-input">
              <img class="icon" src="/<?php echo $path; ?>/images/forms/message.svg">
              <label><?php print t('Email'); ?></label>
              <?php print drupal_render($form['email']); ?>
            </div>
            <div class="custom-input">
              <img class="icon" src="/<?php echo $path; ?>/images/forms/password.svg">
              <label><?php print t('Password'); ?></label>
              <?php print drupal_render($form['password']); ?>
            </div>
          </div>
          <div class="forgot-password-link">
            <a href="https://app.visitor-analytics.io/password/forgot" target="_blank"><?php print t('Forgot password?'); ?></a>
          </div>
          <div class="button-area">
            <?php print drupal_render($form['submit']); ?>
            <div class="register-link-container">
              <i><?php print t('Don\'t have an account? Go to <a class="bold to_register_page" href="/admin/config/services/visitor-analytics/register">Register</a>'); ?></i>
            </div>
          </div>
        </form>
      </div>
      <?php print drupal_render_children($form); ?>
    </div>
  </div>
</div>
